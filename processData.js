const fs = require('fs');

fs.readFile('data.json', 'utf8', (err, data) => {
    if (err) {
        console.error('Error reading file:', err);
        return;
    }

    const jsonData = JSON.parse(data);

    jsonData.newProperty = '----modified from Gitlab Runner';

    const newData = JSON.stringify(jsonData, null, 2);

    fs.writeFile('processedData.json', newData, (err) => {
        if (err) {
            console.error('Error writing file:', err);
            return;
        }
        console.log('Data has been written to new file.');
    });

    console.log(jsonData);
});

